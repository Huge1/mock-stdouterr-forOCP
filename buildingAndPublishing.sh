# v Dockerfile 
 docker build -t stdouterr-mock .
VER=0.3.1 # or `git log |grep commit |wc -l` would work more nicely, perhaps.

# echo docker run --rm -it -p 8080:80 stdouterr-mock>run_local_server_in_docker.sh
# chmod +x run_local_server_in_docker.sh 
 # Publish to dockerHub:
 docker login
 docker tag stdouterr-mock uhuge/stdouterr-mock:$VER
 docker push uhuge/stdouterr-mock:$VER
 #Try:
 docker run -it docker.io/uhuge/stdouterr-mock:$VER
 #gcloud auth login
 #gcloud container clusters get-credentials std-cluster --zone us-central1-a --project driven-pillar-245118
 kubectl run stdouterr-mocking --image docker.io/uhuge/stdouterr-mock:$VER
 #kubectl run stdouterr-mocking --image docker.io/uhuge/stdouterr-mock:$VER --port 8080
