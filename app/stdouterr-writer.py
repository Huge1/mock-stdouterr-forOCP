"""Task of this is to write about 5 stdout something each minute and something to stderr each 5 minutes"""
import random, time, datetime
from datetime import datetime
nowF = datetime.now 
sleep = time.sleep
from sys import stderr
def print_err(*args, **kwargs): 
    print(*args, file=stderr, **kwargs)
#print_err= stderr.write # or logging.warning would be apt alternatives
#                          in the case the custom function above didn't work well, as it seemed.


def myTimeF():
    """ Return formated string of d-a-te n:o:w """
    preciseTime = nowF()
    return preciseTime.strftime("%Y-%m-%d %H:%M:%S")

def randWrites():
    glVar=randWrites.glVar
    print("Standard output #"+str(glVar))
    print("random: "+str(random.randint(1, 1234)), ", time:", myTimeF())
    sleep(1)
    print("random: "+str(random.randint(1, 1234)), ", time:", myTimeF())
    sleep(2)
    print("random: "+str(random.randint(1, 1234)), ", time:", myTimeF())
    sleep(3)
    print("random: "+str(random.randint(1, 1234)), ", time:", myTimeF())
    sleep(4)
    print("random: "+str(random.randint(1, 1234)), ", time:", myTimeF())
    #sleep(5)
    # !Pig's code: Let's inject stderr here:
    if glVar % 5 == 2:
        for aFew in range( random.randint(3, 9) ):
            print_err("Error #", aFew, "!!!...random: ",random.randint(2000, 3234), ", time: ", myTimeF(), sep="")
    randWrites.glVar +=1

randWrites.glVar=1

if __name__ == "__main__":
    #preciseTime = now()
    print("Started!", myTimeF())
    time.sleep(2)
    while(True):
        randWrites()
        sleep(50)
        
    print( "last printed.", myTimeF())
    #Cool to test in vim: :%s/sleep(50)/#/g | :w |:u |!python3 % #The python run needs tuning, do that in a separate console manually.

