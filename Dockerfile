# --- Release with Alpine ----
FROM python:3.6-alpine3.7
# Create app directory
WORKDIR /app

#suboptimal is good enough here...
COPY  /app/ ./
RUN pip install -r requirements.txt
CMD ["python", "stdouterr-writer.py"]
#EXPOSE 80
